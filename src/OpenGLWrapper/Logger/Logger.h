#ifndef LOGGER_H
#define LOGGER_H
#include <string>
#include <iostream>

enum class Level : unsigned char{
  Info, Warning, Error
};



struct Logger{

   template<typename T> static void log(const T& obj, const Level& level = Level::Info){
          using std::cout;

          switch(level){
              case Level::Info:
                cout << "INFO: ";
              break;

              case Level::Warning:
               cout << "WARNING: ";
              break;

              case Level::Error:
               cout << "ERROR: ";
              break;

             default:
               cout << "UNKOWN LEVEL: ";
             break;
         }
       cout << obj << '\n';
   }


};

 #endif

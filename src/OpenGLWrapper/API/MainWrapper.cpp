#include <GL/glew.h>
#include "Utils/APIUtils.h"
#include "../Window/Window.h"
#include "Wrapper.h"
#include "../vendor/stb_image.h"
#include "MainWrapper.h"
#include "APIError.h"

//Local function
const void exit();

const void init(){

   //Initialize apis
  if(!glfwInit()){
    throw APIError(GLFW_INIT_ERROR, "Couldn't initialize GLFW!");
  }

  //Prepare window and create opngl context
  Window::createWindow();

   // Initialize glew and opengl bindings
  glewExperimental = GL_TRUE;
  GLenum state = glewInit();
  if(state == GLEW_ERROR_NO_GL_VERSION){
    string err = "Your graphics card isn't supported/ dosen't work with opengl!";
    err += "GLEW Version: " + std::string((char*)glewGetString(GLEW_VERSION));

    //Exit apis
    glfwTerminate();

    throw APIError(GLEW_INIT_ERROR, err);
  }else if(state != GLEW_OK){

    //Exit apis
    glfwTerminate();

    throw APIError(GLEW_INIT_ERROR, "GLEW Version: " + std::string((char*)glewGetString(GLEW_VERSION)));
  }

  //Opengl wnats the images to be flipped when i load them so i set this flag so when i load an image it'll be automatically flipped
  stbi_set_flip_vertically_on_load(false);

  // Tell opengl to load the tools needed to use 2d textures
  glEnable(GL_TEXTURE_2D);

  // Enable "transparency"
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


}


const void exit(){
  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);

  //Exit apis
  glfwTerminate();
}



const void run(){

  if(Wrapper::setupFunction != nullptr) Wrapper::setupFunction();


  while(!Window::shouldWindowClose()){
     Window::startUpdatingWindow();

     if(Wrapper::renderFunction != nullptr)Wrapper::renderFunction();

     Window::stopUpdatingWindow();
  }

  if(Wrapper::onExitFunction != nullptr)Wrapper::onExitFunction();

  exit();
}

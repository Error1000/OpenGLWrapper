#ifndef MAINUTILS_H
#define MAINUTILS_H

#include <GL/glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/matrix_transform_2d.hpp>

#include "../../renderEngine/Rendering/Image.h"
#include "APIUtils.h"


//For the getCurrentProgramDir function
#ifdef OPENGLWRAPPER_USING_WINDOWS
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif

//To be used by main class
using std::string;
using glm::vec2;
using glm::mat3;
using glm::mat4;
using glm::ortho;
using glm::vec3;
#include <sstream> // for to_string


namespace UserUtils{

inline void backgroundColor(const byte& gray){
    glClearColor((double)gray/255, (double)gray/255, (double)gray/255, 1);
}

inline void backgroundColor(const byte& r, const byte& g, const byte& b){
    glClearColor((double)r/255, (double)g/255, (double)b/255, 1);
}

const string getCurrentProgramDir();


const string readFile(const string& filePath);

const string getRendererInfo();
const string getVendorInfo();
const string getOpenGLVersion();

const Image readImage(const string& filePath);


template<typename T> const string to_string(T val){
std::ostringstream ss;
ss << val;
return ss.str();
}

}

#endif

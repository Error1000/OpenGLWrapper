#include "APIUtils.h"
#include "../../Window/Window.h"
#include <string>
#include <sys/stat.h>


void exitWrapper(){
    Window::setWindowToClose();
}

const bool fileExists(const std::string& filePath){
  struct stat buffer;
  return (stat (filePath.c_str(), &buffer) == 0);
}

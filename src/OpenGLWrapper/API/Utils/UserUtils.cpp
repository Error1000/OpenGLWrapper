#include "UserUtils.h"
#include "../APIError.h"
#include "../../renderEngine/Utils/OGLWError.h"

//TODO: Remove this comment because some people might get offended.
#include <fstream> /* also known as fuck stream */

//For the getCurrentProgramDir function
#include <stdio.h>  /* defines FILENAME_MAX */
#include <sstream> // for to_string

#include "../../vendor/stb_image.h"


const string UserUtils::getCurrentProgramDir(){
  char buff[FILENAME_MAX];
  GetCurrentDir( buff, FILENAME_MAX );
  string current_working_dir(buff);
  return current_working_dir;
}


const string UserUtils::readFile(const string& filePath){
if(!fileExists(filePath))throw APIError(FILE_READING_ERROR, "File: "+filePath+" couldn't be read because it dosen't exist!");
std::ifstream in(filePath);
string content( (std::istreambuf_iterator<char>(in) ),
                    (std::istreambuf_iterator<char>()    ) );
return content;
}

const string UserUtils::getVendorInfo(){
  return string((char*)glGetString(GL_VENDOR));
}

const string UserUtils::getRendererInfo(){
  return string((char*)glGetString(GL_RENDERER));
}


const string UserUtils::getOpenGLVersion(){
  return string((char*)glGetString(GL_VERSION));
}

const Image UserUtils::readImage(const string& filePath){

if(!fileExists(filePath))throw APIError(FILE_READING_ERROR ,"Image: "+ filePath+" dosen't exist!");


Image img;

img.data = stbi_load(filePath.c_str(), &(img.width), &(img.height), &(img.bpp), 4);

return img;
}

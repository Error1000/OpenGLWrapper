#ifndef APIUTILS_H
#define APIUTILS_H
#include "../../Logger/Logger.h"


typedef unsigned char byte;
typedef void(*WrapperVoidFuncPointer)();

void exitWrapper();
const bool fileExists(const std::string& filePath);

#endif

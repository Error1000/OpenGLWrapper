#ifndef APIERROR_H
#define APIERROR_H
#include "../renderEngine/Utils/OGLWError.h"

enum APIErrorType{
 FILE_READING_ERROR,
 GLFW_INIT_ERROR,
 GLEW_INIT_ERROR
};

struct APIError : OGLWError{
const APIErrorType errorType;

explicit APIError(const APIErrorType& et, const std::string& err):
OGLWError(err), errorType(et){}
};

#endif // APIERROR_H

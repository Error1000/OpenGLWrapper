#include "Wrapper.h"
#include "MainWrapper.h"

//Variable declartaions
//Note: We don't use static because we don't want the declaration to only be linked within this context, in other words we want to link this declaration with the Wrapper class definitions so we don't use static
WrapperVoidFuncPointer Wrapper::setupFunction = nullptr;
WrapperVoidFuncPointer Wrapper::renderFunction = nullptr;
WrapperVoidFuncPointer Wrapper::onExitFunction = nullptr;

void Wrapper::runWrapper(){
run();
}

void Wrapper::initWrapper(WrapperVoidFuncPointer setupFunc, WrapperVoidFuncPointer renderFunc){
  Wrapper::setupFunction = setupFunc;
  Wrapper::renderFunction = renderFunc;
  init();

}


void Wrapper::initWrapper(WrapperVoidFuncPointer setupFunc, WrapperVoidFuncPointer renderFunc, WrapperVoidFuncPointer onExitFunc){
  Wrapper::onExitFunction = onExitFunc;
  Wrapper::initWrapper(setupFunc, renderFunc);
}


#ifndef WRAPPER_H
#define WRAPPER_H
#include "Utils/UserUtils.h"

//This class is supposed to be used by the game main source file
class Wrapper{
public:
 //Note: everything after calling startWrapper will be called only after the window closes and the wrapper exits, if you wish to run code before rendering something but after the wrapper initialization, please put it in the setup() function
 //We use static because there can only be one instance of this
 //We put this function in the Game class because it makes sanse that the "Game" startsTheWrapper
 static void initWrapper(WrapperVoidFuncPointer setupFunc, WrapperVoidFuncPointer renderFunc);
 static void initWrapper(WrapperVoidFuncPointer setupFunc, WrapperVoidFuncPointer renderFunc, WrapperVoidFuncPointer onExitFunc);
 static void runWrapper();

 //Variable definitions
 static WrapperVoidFuncPointer setupFunction;
 static WrapperVoidFuncPointer renderFunction;
 static WrapperVoidFuncPointer onExitFunction;

};


#endif


#ifndef WINDOWERROR_H
#define WINDOWERROR_H
#include "../renderEngine/Utils/OGLWError.h"

enum WindowErrorType{
WINDOW_CREATION_ERROR,
FUNCTION_NOT_AVAILABLE,
WINDOW_SIZE_ERROR
};

struct WindowError : OGLWError{
const WindowErrorType errorType;

explicit WindowError(const WindowErrorType& et, const std::string err):
OGLWError(err), errorType(et){}
};

#endif // WINDOWERROR_H

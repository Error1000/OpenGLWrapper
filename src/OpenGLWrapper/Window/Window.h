#ifndef WINDOW_H
#define WINDOW_H

#include <GL/glew.h>
#include <string>
#include <GLFW/glfw3.h>
#include <glm/vec2.hpp>


struct Window {
   private:

    //Variable declarations
    static int width;
    static int height;
    static int SCREEN_WIDTH;
    static int SCREEN_HEIGHT;

	const static int minWidth;
	const static int minHeight;
    static std::string name;
    static bool isFullscreen;
    static bool isInitialized;
    static GLFWwindow* window;
    static GLFWvidmode* monitor;
    static bool resizeable;

     //FPS stuff
    static double lastSecond;
    static double lastTime;
    static int fps;
    static int nbFrames;
    static bool vsync;

    static double deltaTime;
    static double totalDeltas;
    static unsigned int deltas;
    static void(*onResizeFunc)();

   public:

    //Function declarations
    static void createWindow();
    static void startUpdatingWindow();
    static void stopUpdatingWindow();


    inline static void setWindowToClose(){ glfwSetWindowShouldClose(Window::window, GLFW_TRUE); }
    inline static bool shouldWindowClose(){ return glfwWindowShouldClose(Window::window); }

    static void setResizeable(const bool resizeable);
    static void setWindowResizeCallback(void(*func)());
    static void setHeight(const int height);
    static void setWidth(const int width);
    static void setWindowSize(const int width, const int height);
    inline static const int getWidth(){ return Window::width; }
    inline static const int getHeight(){ return Window::height; }
    static const int getScreenWidth();
    static const int getScreenHeight();

    static void setTitle(const std::string& title);
    inline const static std::string getTitle(){ return Window::name; }

    static void useVSync(const bool vsync);

    static void setFullScreen(const bool fullscreen);

    inline const static double getAverageDelta(){ return totalDeltas / deltas; }
    inline const static double getDelta(){ return Window::deltaTime; }
    inline const static bool isKeyPressed(const int& keyCode){ return (glfwGetKey(Window::window, keyCode) == GLFW_TRUE); }
    inline const static bool isMouseKeyPressed(const int& keyCode){ return (glfwGetMouseButton(Window::window, keyCode) == GLFW_PRESS); }
    inline const static glm::vec2 getMousePos(){
     double xpos, ypos;
     glfwGetCursorPos(Window::window, &xpos, &ypos);
     return glm::vec2(xpos, ypos);
    };

    inline const static int getFPS(){ return Window::fps; }
    inline const static double getCurrentTime(){ return glfwGetTime(); }


    static GLFWwindow* getWindow();

};


#endif

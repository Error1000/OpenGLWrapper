#include "../Logger/Logger.h"
#include "Window.h"
#include "WindowError.h"
#include <sstream>

using std::string;

//Definitions of variables
//These are just default values of static/const variables
//These are in this source file becuase if they were in the headrer file, if the header file got included twice the variables would be defined twice
//Always remember delecrations can be in multiple source files but there mus only be one definition
int Window::width = 400;
int Window::height = 400;
const int Window::minWidth = 100;
const int Window::minHeight = 100;
string Window::name = "Window";
bool Window::isFullscreen = false;
bool Window::isInitialized = false;
GLFWwindow* Window::window;
GLFWvidmode* Window::monitor;
int Window::SCREEN_WIDTH;
int Window::SCREEN_HEIGHT;
bool Window::resizeable = false;


double Window::lastTime = glfwGetTime();
double Window::lastSecond = glfwGetTime();
int Window::fps = 0;
int Window::nbFrames = 0;
bool Window::vsync = true;
void(*Window::onResizeFunc)() = nullptr;

double Window::deltaTime = 0;
double Window::totalDeltas = 0;
unsigned int Window::deltas = 0;

void Window::createWindow(){

	    //Initialize some variables from the public variables
        Window::monitor = (GLFWvidmode*)glfwGetVideoMode(glfwGetPrimaryMonitor());
        Window::SCREEN_WIDTH = (*Window::monitor).width;
        Window::SCREEN_HEIGHT = (*Window::monitor).height;

      	//Just to make sure that the what the game engine thinks is the window size is the actual window size
		//This is done just because it is used in calculations and it's a good idea for the two things to be the same
		//Oh, btw this is here just in case someone sets the width and height of the window and also makes it full-screen at the same time

		if (isFullscreen) {
			Window::width =  Window::SCREEN_WIDTH;
			Window::height = Window::SCREEN_HEIGHT;
		}

		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);


		glfwWindowHint(GLFW_RESIZABLE, Window::resizeable ? GLFW_TRUE : GLFW_FALSE);

       //Let glfw know some things about the context
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		Window::window = glfwCreateWindow(Window::width, Window::height, Window::name.c_str(), Window::isFullscreen ? glfwGetPrimaryMonitor() : NULL, NULL);

        if (window == NULL){
            throw WindowError(WINDOW_CREATION_ERROR, "GLFW failed to create window!");
		}

		glfwMakeContextCurrent(Window::window);

		// Set minimum width and height the window can be
		glfwSetWindowSizeLimits(Window::window, Window::minWidth, Window::minHeight, GLFW_DONT_CARE, GLFW_DONT_CARE);

        //Set resize callback
        ///Note: Having the parameter win is requierd as glfw won't let me remove it
        glfwSetWindowSizeCallback(Window::window, [](GLFWwindow *win, int newWidth, int newHeight){
                                    ///Note: Calling setWidth()/setHeight() here would just waste cpu cycles

                                    Window::width = newWidth;
                                    Window::height = newHeight;
                                    glViewport(0, 0, Window::getWidth(), Window::getHeight());
                                    if(Window::onResizeFunc != nullptr)Window::onResizeFunc();
        });


		glfwSetWindowPos(Window::window, (Window::SCREEN_WIDTH - Window::width) / 2, (Window::SCREEN_HEIGHT - Window::height) / 2);

		// Finally we show the window
		glfwShowWindow(Window::window);



		Window::isInitialized = true;

		//Set vsync
        if(vsync)glfwSwapInterval(1); else glfwSwapInterval(0);

}


void Window::startUpdatingWindow(){
    // Stuff to do with frame rate
    const double currentTime = glfwGetTime();
	Window::deltaTime = currentTime - Window::lastTime;
	Window::totalDeltas += Window::deltaTime;
	Window::deltas++;

	Window::nbFrames++;
	if (currentTime - Window::lastSecond >= 1.0) {
		// If more that 1 second passed
		Window::fps = Window::nbFrames;
		Window::nbFrames = 0;
		Window::lastSecond = currentTime;
	}

	Window::lastTime = currentTime;



	// Pull new events / update buffer(context)
	glfwPollEvents();

	// Clear last context("canvas"/frame)
	glClear(GL_COLOR_BUFFER_BIT);

}



void Window::stopUpdatingWindow(){
	//Swap buffers
	glfwSwapBuffers(window);
}



void Window::setResizeable(const bool isResizeable){
if (isInitialized){
    throw WindowError(FUNCTION_NOT_AVAILABLE, "The window can't be set to be resizeable after the wrapper has started, but you can use the setWidth, setHeight, setTitle, etc. methods even after the wrapper has started!");
    return;
}

 Window::resizeable = isResizeable;

}

void Window::setWindowResizeCallback(void(*func)()){
  Window::onResizeFunc = func;
}

//Definitions of methods
void Window::setWidth(const int newWidth){

        if (newWidth >= Window::minWidth) {
			Window::width = newWidth;
			if(Window::isInitialized)glfwSetWindowSize(Window::window, newWidth, Window::height);
		}else {
            std::ostringstream ss;
            ss << Window::minWidth;
			throw WindowError(WINDOW_SIZE_ERROR, "The width of the window should be grater than or at least equal to "+ ss.str() + " !");
		}

}

void Window::setHeight(const int newHeight){

	    if (newHeight >= Window::minHeight) {
			Window::height = newHeight;
			if(Window::isInitialized)glfwSetWindowSize(Window::window, Window::width, newHeight);
		}else {
		    std::ostringstream ss;
		    ss << Window::minHeight;
			throw WindowError(WINDOW_SIZE_ERROR, "The height of the window should be grater than or at least equal to "+ ss.str() + " !");
		}
}

const int Window::getScreenHeight(){
 if(Window::isInitialized)
    return Window::SCREEN_HEIGHT;
 else{
    throw WindowError(FUNCTION_NOT_AVAILABLE, "This function can only be called after the wrapper has started!");
    return 0;
 }

}

const int Window::getScreenWidth(){
 if(Window::isInitialized)
    return Window::SCREEN_WIDTH;
 else{
    throw WindowError(FUNCTION_NOT_AVAILABLE, "This function can only be called after the wrapper has started!");
    return 0;
 }

}


void Window::setWindowSize(const int newWidth, const int newHeight){

     if (newWidth >= Window::minWidth && newHeight >= Window::minHeight) {
			Window::width = newWidth;
			Window::height = newHeight;
			if(Window::isInitialized)glfwSetWindowSize(Window::window, newWidth, newHeight);
		}else {
		    std::ostringstream ss1;
		    std::ostringstream ss2;
		    ss1 << Window::minWidth;
		    ss2 << Window::minHeight;
			throw WindowError(WINDOW_SIZE_ERROR, "The height or width of the window should be grater than or at least equal to "+ ss1.str() + " and " + ss2.str() + " !");
		}
}

void Window::setTitle(const string& title){
   Window::name = title;
   if(Window::isInitialized)glfwSetWindowTitle(Window::window, Window::name.c_str());
}

void Window::setFullScreen(const bool fullscreen){
		if (isInitialized){
			throw WindowError(FUNCTION_NOT_AVAILABLE, "The window can't be set to fullscren after the wrapper has started, but you can use the setWidth, setHeight, setTitle, etc. methods even after the wrapper has started!");
			return;
		}
       Window::isFullscreen = fullscreen;
}

void Window::useVSync(const bool newVSync){
        if (isInitialized){
			throw WindowError(FUNCTION_NOT_AVAILABLE, "The window can't be set to use vsync after the wrapper has started, but you can use the setWidth, setHeight, setTitle, etc. methods even after the wrapper has started!");
			return;
		}

        Window::vsync = newVSync;
}

GLFWwindow* Window::getWindow(){
	if(Window::window == nullptr)throw WindowError(WINDOW_CREATION_ERROR, "Couldn't get window becuase it doesn't exist!");
	return Window::window;
}


#ifndef GAME_OBJECT_2D_H
#define GAME_OBJECT_2D_H


#include "DrawableObject.h"


#include <glm/vec2.hpp>
#include <glm/gtx/matrix_transform_2d.hpp>



class DrawableObject2D : public DrawableObject<glm::mat3, glm::vec2>{
   private:
     glm::vec2 pos = glm::vec2(0, 0);
     glm::vec2 size = glm::vec2(1, 1);
     float rotation = 0;

   public:
     inline explicit DrawableObject2D(Shape* shape, Shader* shader):
     DrawableObject(shape, shader){
      //Prepare matrix
      DrawableObject2D::modelMatrix = glm::scale(DrawableObject2D::modelMatrix, glm::vec2(0.5f, 0.5f));
     }

    ~DrawableObject2D() override{} //Override makes this code more future proof, in case i decide to actually add a cosntructor, even if ther's no point right now

     inline const glm::vec2 getPosition() const override{ return DrawableObject2D::pos; }
     const glm::vec2 getSize() const override;
     inline const float getRotation() const override{ return DrawableObject2D::rotation; }

     void setPosition(const glm::vec2& pos) override;
     void move(const glm::vec2& dist) override;
     void setSize(const glm::vec2& siz) override;
     void setRotation(const float r) override;
     void rotate(const float r) override;
};

#endif

#include "DrawableObject2D.h"
#include "../../Window/Window.h"

#include <glm/gtx/matrix_transform_2d.hpp>
#include "../../Logger/Logger.h"
#include <glm/vec3.hpp>

using glm::vec2;
using glm::vec3;

const glm::vec2 DrawableObject2D::getSize() const{
 return DrawableObject2D::size;
}


void DrawableObject2D::setSize(const vec2& siz){
    if(siz.x > 0.0f || siz.y > 0.0f){
      if(DrawableObject2D::size != glm::vec2(0, 0)) DrawableObject2D::modelMatrix = glm::scale(DrawableObject2D::modelMatrix, vec2(1,1)/DrawableObject2D::size);

      DrawableObject2D::size = siz;

      DrawableObject2D::modelMatrix = glm::scale(DrawableObject2D::modelMatrix, DrawableObject2D::size);
  }else{

      if(DrawableObject2D::size != glm::vec2(0, 0)) DrawableObject2D::modelMatrix = glm::scale(DrawableObject2D::modelMatrix, vec2(1,1)/DrawableObject2D::size);

      DrawableObject2D::size = vec2(1, 1);

      DrawableObject2D::modelMatrix = glm::scale(DrawableObject2D::modelMatrix, DrawableObject2D::size);

  }

}


void DrawableObject2D::setPosition(const vec2& newPos){
    //Revert rotation
    DrawableObject2D::modelMatrix = glm::rotate(DrawableObject2D::modelMatrix, -DrawableObject2D::rotation);

    //Revert positon
    DrawableObject2D::modelMatrix = glm::translate(DrawableObject2D::modelMatrix, -(DrawableObject2D::pos / DrawableObject2D::size * vec2(2, 2)) );

    //And move to new position
    DrawableObject2D::pos = newPos;

    DrawableObject2D::modelMatrix = glm::translate(DrawableObject2D::modelMatrix, DrawableObject2D::pos / DrawableObject2D::size * vec2(2, 2));

    //Reset rotation
    DrawableObject2D::modelMatrix = glm::rotate(DrawableObject2D::modelMatrix, DrawableObject2D::rotation);
}

void DrawableObject2D::move(const vec2& dist){
    //Revert rotation
    DrawableObject2D::modelMatrix = glm::rotate(DrawableObject2D::modelMatrix, -DrawableObject2D::rotation);
    DrawableObject2D::modelMatrix = glm::translate(DrawableObject2D::modelMatrix, dist / DrawableObject2D::size * vec2(2, 2));
    DrawableObject2D::pos = DrawableObject2D::pos + dist;
    //Reset rotation
    DrawableObject2D::modelMatrix = glm::rotate(DrawableObject2D::modelMatrix, DrawableObject2D::rotation);
}


void DrawableObject2D::setRotation(const float r){
    //Revert rotation
    DrawableObject2D::modelMatrix = glm::rotate(DrawableObject2D::modelMatrix, -DrawableObject2D::rotation);
    DrawableObject2D::rotation = r;
    //Reset rotation
    DrawableObject2D::modelMatrix = glm::rotate(DrawableObject2D::modelMatrix, DrawableObject2D::rotation);
}


void DrawableObject2D::rotate(const float r){
    DrawableObject2D::rotation += r;
   //Rotate
   DrawableObject2D::modelMatrix = glm::rotate(DrawableObject2D::modelMatrix, r);
}



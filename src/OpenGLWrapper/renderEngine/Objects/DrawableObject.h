#ifndef DRAWABLEOBJECT_H
#define DRAWABLEOBJECT_H
#include "../Rendering/Shader.h"
#include "../Shape/Shape.h"
#include "../../Logger/Logger.h"
#include "../../Window/Window.h"


template<typename mat, typename vec>
class DrawableObject{
  protected:
    mat modelMatrix = mat(1);
    Shape* objectShape;
    Shader* objectShader;

  public:
     explicit DrawableObject(Shape* shape, Shader* shader):
      objectShape(shape), objectShader(shader){

        if(DrawableObject::objectShape == nullptr){
         Logger::log("Shape can't be null!",Level::Error);
         Window::setWindowToClose();
        }

        if(DrawableObject::objectShader == nullptr){
         Logger::log("Shader can't be set to null!", Level::Error);
         Window::setWindowToClose();
        }

     }

     virtual ~DrawableObject(){} //Must have definition, even if it could be pure and virtual

     inline void bindObjectShape() const{ DrawableObject::objectShape->bindShape(); }
     inline void bindObjectShader() const{ DrawableObject::objectShader->bindShader(); }
     void renderObject() const{ DrawableObject::objectShape->renderShapeWithBoundShader(); }
     inline void unbindObjectShape() const{ DrawableObject::objectShape->unbindShape(); }
     inline void unbindObjectShader() const{ DrawableObject::objectShader->unbindShader(); }

     virtual const vec getSize() const = 0;
     virtual const vec getPosition() const = 0;
     virtual const float getRotation() const = 0;

     virtual void setPosition(const vec& pos) = 0;
     virtual void move(const vec& dist) = 0;
     virtual void setSize(const vec& siz) = 0;
     virtual void setRotation(const float r) = 0;
     virtual void rotate(const float r) = 0;

     inline mat& getModelMatrix(){ return DrawableObject::modelMatrix; }
     inline Shader* getShader() const{ return DrawableObject::objectShader; }
     inline Shape* getShape() const{ return DrawableObject::objectShape; }
     inline void setShape(Shape *s){ DrawableObject::objectShape = s;}
     inline void setShader(Shader *sh){ DrawableObject::objectShader = sh;}

};

#endif

#ifndef SHADERERROR_H
#define SHADERERROR_H
#include "../Utils/OGLWError.h"

enum ShaderErrorType{
VSHADER_COMPILATION_ERR,
FSHADER_COMPILATION_ERR,
SHADER_LINKING_ERR,
SHADER_VALIDATION_ERR,
UNIFORM_NOT_FOUND_ERR
};

struct ShaderError : OGLWError{
const ShaderErrorType errorType;

explicit ShaderError(const ShaderErrorType& et, const std::string& err):
OGLWError(err), errorType(et){}
};

#endif // SHADERERROR_H

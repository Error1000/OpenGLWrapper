#include "Animation.h"
#include "../../Window/Window.h"

Animation::Animation(Texture** animFrames,const unsigned int& framesSiz,const unsigned int& animFps):
frames(animFrames), framesSize(framesSiz), animationFps(animFps), elapsedTime(animFps - 1.0 / animFps), lastTime(Window::getCurrentTime()){}


void Animation::bindAnimation(const GLuint samplerId){
		Animation::currentTime = Window::getCurrentTime();
		Animation::elapsedTime += Animation::currentTime - Animation::lastTime;

		if (Animation::elapsedTime >= Animation::animationFps) {
			Animation::elapsedTime -= 1.0 / Animation::animationFps;
			Animation::currentFrame++;
		}

		if (Animation::currentFrame >= Animation::framesSize)
			Animation::currentFrame = 0;

		Animation::lastTime = Animation::currentTime;

		Animation::frames[Animation::currentFrame]->bindTexture(samplerId);

}


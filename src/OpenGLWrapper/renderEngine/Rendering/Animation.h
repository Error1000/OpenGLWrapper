#ifndef ANIMATION_H
#define ANIMATION_H
#include "Texture.h"

class Animation{
protected:
    Texture** frames;
    unsigned int framesSize = 0;
    const unsigned int animationFps;
    unsigned int currentFrame = 0;

    double elapsedTime, currentTime = 0, lastTime;

public:
    explicit Animation(Texture** animFrames, const unsigned int& framesSiz, const unsigned int& animFps);

    void bindAnimation(const GLuint samplerId);

    inline const unsigned int getCurrentFrame() const{ return Animation::currentFrame; }

};

#endif

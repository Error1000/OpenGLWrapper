#ifndef TEXTURE_H
#define TEXTURE_H
#include <GL/glew.h>
#include <string>
#include "Image.h"

class Texture{
  private:
    unsigned int id;
    int height, width, bpp;

  public:
    explicit Texture(const Image& img, const GLint minFilter = GL_LINEAR, const GLint magFilter = GL_LINEAR, const GLint xWrap = GL_CLAMP_TO_EDGE, const GLint yWrap = GL_CLAMP_TO_EDGE);
    inline ~Texture(){ glDeleteTextures(1, &(Texture::id)); }

    void bindTexture(const GLuint& samplerId) const;
    inline void unbindTexture() const{ glBindTexture(GL_TEXTURE_2D, 0); }

    inline const int getWidth() const{ return Texture::width; }
    inline const int getHeight() const{ return Texture::height; }
};

#endif

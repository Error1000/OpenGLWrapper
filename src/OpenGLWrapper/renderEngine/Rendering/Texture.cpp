#include "Texture.h"
#include <GL/glew.h>

Texture::Texture(const Image& img, const GLint minFilter, const GLint magFilter, const GLint xWrap, const GLint yWrap):
height(img.height), width(img.width), bpp(img.bpp){

glGenTextures(1, &(Texture::id));
glBindTexture(GL_TEXTURE_2D, Texture::id);

glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, xWrap);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, yWrap);

glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, img.width, img.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, img.data);

Texture::unbindTexture();

}

void Texture::bindTexture(const GLuint& samplerId) const{

glActiveTexture(GL_TEXTURE0+samplerId);
glBindTexture(GL_TEXTURE_2D, Texture::id);

}

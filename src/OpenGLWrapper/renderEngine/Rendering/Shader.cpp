#include "Shader.h"
#include <vector>
#include <sstream>
#include <GL/glew.h>
#include "ShaderError.h"
#include "../../Logger/Logger.h"
#include "../../Window/Window.h"


using std::string;
using std::vector;

Shader::Shader(const string& vertexShader, const string& fragmentShader){

    //Create shader prgoram
    programId = glCreateProgram();


    //Load vertex shader

    // We allocate space for the shaders and and get it's id and set it to vsId
    Shader::vsId = glCreateShader(GL_VERTEX_SHADER);

    // We set the vertex shader that has the id of vsId to what we get from the user
    const char* vsc_str = vertexShader.c_str();
    glShaderSource(Shader::vsId, 1, &vsc_str, nullptr);

    // Now we compile the source attached to the shader with the id vsId
    glCompileShader(Shader::vsId);


    GLint vertexShader_compiled;
    glGetShaderiv(Shader::vsId, GL_COMPILE_STATUS, &vertexShader_compiled);
    // Check for errors when compiling the shader
    if (vertexShader_compiled != GL_TRUE) {
        GLint logLength = 0;
        glGetShaderiv(Shader::vsId, GL_INFO_LOG_LENGTH, &logLength);

        vector<char> errorLog(logLength);
        glGetShaderInfoLog(Shader::vsId, logLength, &logLength, &errorLog[0]);
        string errorStr(errorLog.begin(), errorLog.end());
        throw ShaderError(VSHADER_COMPILATION_ERR, errorStr);
    }

    // Load the fragment shader

    // We allocate space for the shaders and and get it's id and set it to fsId
    Shader::fsId = glCreateShader(GL_FRAGMENT_SHADER);

    // We set the vertex shader that has the id of fsId to what we got from the user
    const char* fsc_str = fragmentShader.c_str();
    glShaderSource(Shader::fsId, 1, &fsc_str, nullptr);

    // Now we compile the source attached to the shader with the id fsId
    glCompileShader(Shader::fsId);

    GLint fragmentShader_compiled;
    glGetShaderiv(Shader::fsId, GL_COMPILE_STATUS, &fragmentShader_compiled);

    // Check for errors when compiling the shader
    if (fragmentShader_compiled != GL_TRUE) {
        GLint logLength = 0;
        glGetShaderiv(Shader::fsId, GL_INFO_LOG_LENGTH, &logLength);

        vector<char> errorLog(logLength);
        glGetShaderInfoLog(Shader::fsId, logLength, &logLength, &errorLog[0]);
        string errorStr(errorLog.begin(), errorLog.end());
        throw ShaderError(FSHADER_COMPILATION_ERR, errorStr);
    }


    // Assign the two unique shader ids to one unique program id
    glAttachShader(Shader::programId, Shader::vsId);
    glAttachShader(Shader::programId, Shader::fsId);

    // Finally we link the vertexShader and the fragmentShader together so that tey
    // can talk and give the vertex shader the vertices array
    glLinkProgram(Shader::programId);

    // And we check for errors during the linking process, in case something went
    // wrong and one shader tried to communicate in some weird way with the other or
    // there are some communicating problems

    GLint shader_linked = 1;
    glGetProgramiv(Shader::programId, GL_LINK_STATUS, &shader_linked);
    if (shader_linked != GL_TRUE) {
        GLint logLength = 0;
        glGetProgramiv(Shader::programId, GL_INFO_LOG_LENGTH, &logLength);

        vector<char> errorLog(logLength);
        glGetProgramInfoLog(Shader::programId, logLength, &logLength, &errorLog[0]);
        string errorStr(errorLog.begin(), errorLog.end());
        throw ShaderError(SHADER_LINKING_ERR, errorStr);
    }


    glDetachShader(Shader::programId, Shader::vsId);
    glDetachShader(Shader::programId, Shader::fsId);

    glDeleteShader(Shader::vsId);
    glDeleteShader(Shader::fsId);
}


void Shader::validateShader() const{

   // Now we validate that everything went okay
    glValidateProgram(Shader::programId);

    // And now, just in case one final check just to make sure, you know, you can
    // never be sure in programming
    // This will check if somehow linking and compiling went okay but we couldn't
    // validate the program so this is in case some stuff is wrong but successfully
    // compiled
    GLint shader_validated;
    glGetProgramiv(Shader::programId, GL_VALIDATE_STATUS, &shader_validated);
    if (shader_validated != GL_TRUE) {
        GLint logLength = 0;
        glGetProgramiv(Shader::programId, GL_INFO_LOG_LENGTH, &logLength);

        vector<char> errorLog(logLength);
        glGetProgramInfoLog(Shader::programId, logLength, &logLength, &errorLog[0]);
        string errorStr(errorLog.begin(), errorLog.end());
        throw ShaderError(SHADER_VALIDATION_ERR, errorStr);
    }

}


void Shader::loadUniformId(const string& name){
  const int uniformId = glGetUniformLocation(Shader::programId, name.c_str());
  // This will test if we could actually find the variable in the shader code
  if(uniformId == -1){
     std::ostringstream ss;
     ss << Shader::programId;
     throw ShaderError(UNIFORM_NOT_FOUND_ERR, "Couldn't find uniform id of uniform: " + name + " in shader:" + ss.str() + "!");
  }
    Shader::uniformIds[name] = uniformId;
}

void Shader::loadAttributeId(const std::string& name){
    const int attribId = glGetAttribLocation(Shader::programId, name.c_str());
    if(attribId == -1){
      std::ostringstream ss;
      ss << Shader::programId;
      throw ShaderError(UNIFORM_NOT_FOUND_ERR, "Couldn't find attribute id of attribute: " + name + " in shader:" + ss.str() + "!");
    }
    Shader::attributeIds[name] = attribId;
}

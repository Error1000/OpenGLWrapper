#ifndef SHADER_H
#define SHADER_H
#include <GL/glew.h>
#include <string>
#include <map>
#include <glm/detail/type_mat.hpp>
#include <glm/gtc/type_ptr.hpp>



class Shader{
  private:
    unsigned int vsId, fsId, programId;
    std::map<std::string, int> uniformIds;
    std::map<std::string, int> attributeIds;

  public:
    Shader(const std::string& vertexShader, const std::string& fragmentShader);

    void validateShader() const;
    void loadUniformId(const std::string& name);
    inline const int getUniformId(const std::string& name){ return uniformIds[name]; }

    inline void setUniform(const int uniformId, const int value){ glUniform1i(uniformId, value); }
    inline void setUniform(const int uniformId, const float value){ glUniform1f(uniformId, value); }
    inline void setMatrixUniform(const int uniformId, const glm::mat4 value){ glUniformMatrix4fv(uniformId, 1, GL_FALSE, glm::value_ptr(value)); }
    inline void setMatrixUniform(const int uniformId, const glm::mat3 value){ glUniformMatrix3fv(uniformId, 1, GL_FALSE, glm::value_ptr(value)); }

    void loadAttributeId(const std::string& name);
    inline const int getAttributeId(const std::string& name){ return attributeIds[name]; }

    inline void bindShader() const{ glUseProgram(Shader::programId); }
    void unbindShader() const{ glUseProgram(0); }
    inline void deleteShader() const{ glDeleteProgram(Shader::programId); }
};

#endif

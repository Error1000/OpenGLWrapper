#ifndef VAO_H
#define VAO_H
#include <GL/glew.h>
#include <list>

class VAO{
   private:
     GLuint id, boundIndex;
     std::list<GLuint> attribIds;

   public:
    inline VAO(){ glGenVertexArrays(1, &(VAO::id)); }
    inline ~VAO(){ glDeleteVertexArrays(1, &(VAO::id)); }

    inline void bindVAO() const{ glBindVertexArray(VAO::id); }
    inline void unbindVAO() const{ glBindVertexArray(0); }

    //Enable the modification of all of the arrays stored in the vao
    inline void bindAttribArrays(){ for(GLuint attribId : VAO::attribIds){ glEnableVertexAttribArray(attribId); } }

    //Enable the modification of one array with the id of attribId in the vao
    inline void bindAttribArray(GLuint attribId) { glEnableVertexAttribArray(attribId); VAO::boundIndex = attribId; }


    //Disable the modification of one array with the id of attribId in the vao
    inline void unbindAttribArray(GLuint attribId) const{ glDisableVertexAttribArray(attribId); }


    //Disable the modification of all of the arrays stored in the vao
    inline void unbindAttribArrays(){ for(GLuint attribId : VAO::attribIds){ glDisableVertexAttribArray(attribId); } }

    inline const std::list<GLuint> getArrtibArrayIds()const{ return VAO::attribIds; }


    inline void mapBoundVBOToAttribArray(GLenum dataType, const GLuint attribsPerVertex){ glVertexAttribPointer(VAO::boundIndex, attribsPerVertex, dataType, GL_FALSE, 0, NULL); VAO::attribIds.push_back(VAO::boundIndex);}

};

#endif

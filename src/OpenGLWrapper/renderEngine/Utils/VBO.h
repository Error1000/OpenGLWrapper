#ifndef VBO_H
#define VBO_H
#include <GL/glew.h>

class VBO{
    private:
       GLuint id;
       GLsizei buffLength;

    public:
      inline VBO(){ glGenBuffers(1, &(VBO::id) ); }
      inline ~VBO(){ glDeleteBuffers(VBO::buffLength, &(VBO::id)); }

    inline void bindVBO() const{ glBindBuffer(GL_ARRAY_BUFFER, VBO::id); }
    inline void unbindVBO() const{ glBindBuffer(0, VBO::id); }

    template<typename T> void uploadData(const T data[], const GLsizei dataLength, const GLenum usage){
        glBufferData(GL_ARRAY_BUFFER, dataLength * sizeof(T), data, usage);
        VBO::buffLength = dataLength;
     }


    //Note: The offset parameter is an integer offset into the buffer object where we should begin updating. The size parameter is the number of bytes we should copy out of data.
    template<typename T> void uploadSubData( const T data[], const GLsizei dataLength, const unsigned int offset){
        glBufferSubData(GL_ARRAY_BUFFER, offset, dataLength * sizeof(T), data);
        VBO::buffLength = dataLength;
     }


      inline GLuint* getId(){ return &(VBO::id); }

      inline const GLsizei getSize() const{ return VBO::buffLength; }

};

#endif

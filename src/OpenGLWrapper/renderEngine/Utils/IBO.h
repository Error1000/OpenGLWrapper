#ifndef IBO_H
#define IBO_H

#include <GL/glew.h>

class IBO{
    private:
       GLuint id;
       GLsizei buffLength;

    public:
      inline IBO(){ glGenBuffers(1, &(IBO::id) ); }
      inline ~IBO(){ glDeleteBuffers(IBO::buffLength, &(IBO::id)); }

    inline void bindIBO() const{ glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO::id); }
    inline void unbindIBO() const{ glBindBuffer(0, IBO::id); }

    template<typename T> void uploadData(const T data[], const GLsizei dataLength, const GLenum usage){
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, dataLength * sizeof(T), data, usage);
        IBO::buffLength = dataLength;
     }


    //Note: The offset parameter is an integer offset into the buffer object where we should begin updating. The size parameter is the number of bytes we should copy out of data.
    template<typename T> void uploadSubData( const T data[], const GLsizei dataLength, const unsigned int offset){
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offset, dataLength * sizeof(T), data);
        IBO::buffLength = dataLength;
     }


      inline GLuint* getId(){ return &(IBO::id); }

      inline const GLsizei getSize() const{ return IBO::buffLength; }

};
#endif // IBO_H_INCLUDED

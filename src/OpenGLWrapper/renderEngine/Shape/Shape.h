#ifndef SHAPE_H
#define SHAPE_H
#include "../Utils/VBO.h"
#include "../Utils/IBO.h"
#include "../Utils/VAO.h"


class Shape{
   protected:
     VBO *verticesPosVBO;
     IBO *indicesIBO;
     VAO *shapeVAO;

     bool createdvpVBO = false, creatediIBO = false, createdshVAO = false;

   public:
     explicit Shape(VBO* verticesPos, const int posDimension, const int vertPosAttribArrayId, IBO* indices);
     explicit Shape(const float verticiesPos[], const unsigned int verticiesPosLen, const int vertPosAttribArrayId, const int posDimension, GLenum vertPosUsage, const int indices[], const unsigned int indicesLen, GLenum indicesUsage);

     virtual inline ~Shape() {
         if(Shape::createdvpVBO)delete Shape::verticesPosVBO;
         if(Shape::creatediIBO)delete Shape::indicesIBO;
         if(Shape::createdshVAO)delete Shape::shapeVAO;
     }

     void bindShape() const;
     void renderShapeWithBoundShader() const;
     void unbindShape() const;
     inline VAO* getVAO() const{ return Shape::shapeVAO; }
     inline VBO* getVertPosVBO() const{ return Shape::verticesPosVBO; }
     inline IBO* getVertIndicesIBO() const{ return Shape::indicesIBO; }

};

#endif

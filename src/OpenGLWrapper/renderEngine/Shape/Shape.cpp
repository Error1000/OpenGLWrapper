#include "Shape.h"
#include "../../Logger/Logger.h"


Shape::Shape(const float verticiesPos[], const unsigned int verticiesPosLen, const int vertPosAttribArrayId, const int posDimension, GLenum vertPosUsage, const int indices[], const unsigned int indicesLen, GLenum indicesUsage){
   Shape::verticesPosVBO = new VBO();
   Shape::verticesPosVBO->bindVBO();
   Shape::verticesPosVBO->uploadData(verticiesPos, verticiesPosLen, vertPosUsage);
   Shape::createdvpVBO = true;


   Shape::shapeVAO = new VAO();
   Shape::shapeVAO->bindVAO();
   Shape::shapeVAO->bindAttribArray(vertPosAttribArrayId);

   //This is here just to make sure the right vbo is uploaded cuz why not
   Shape::verticesPosVBO->bindVBO();
   Shape::shapeVAO->mapBoundVBOToAttribArray(GL_FLOAT, posDimension);

   Shape::shapeVAO->unbindAttribArray(vertPosAttribArrayId);
   Shape::createdshVAO = true;


   Shape::indicesIBO = new IBO();
   Shape::indicesIBO->bindIBO();
   Shape::indicesIBO->uploadData(indices, indicesLen, indicesUsage);
   Shape::creatediIBO = true;

}

Shape::Shape(VBO* verticesPos, const int posDimension, const int vertPosAttribArrayId, IBO* indices):
verticesPosVBO(verticesPos), indicesIBO(indices){
    Shape::shapeVAO = new VAO();
    Shape::shapeVAO->bindVAO();
    Shape::shapeVAO->bindAttribArray(vertPosAttribArrayId);
    Shape::verticesPosVBO->bindVBO();
    Shape::shapeVAO->mapBoundVBOToAttribArray(GL_FLOAT, posDimension);
    Shape::shapeVAO->unbindAttribArray(vertPosAttribArrayId);
    Shape::createdshVAO = true;
}


void Shape::bindShape() const{
     //For shader
     Shape::shapeVAO->bindVAO();
     Shape::shapeVAO->bindAttribArrays();

     //For rendring
     Shape::indicesIBO->bindIBO();

}

void Shape::renderShapeWithBoundShader() const{
    glDrawElements(GL_TRIANGLES, indicesIBO->getSize(), GL_UNSIGNED_INT, (void*)0);
}

void Shape::unbindShape() const{
    Shape::shapeVAO->unbindVAO();
    Shape::shapeVAO->unbindAttribArrays();

    Shape::indicesIBO->unbindIBO();
}



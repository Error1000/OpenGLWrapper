#include "TexturedShape.h"

TexturedShape::TexturedShape(const float verticiesPos[], const unsigned int verticiesPosLen, const int vertPosAttribIndex, const int posDimension, GLenum vertPosUsage, const float vertTexCoords[], const unsigned int vertTexCoordsLen, const int vertTexCoordsAttribIndex, const int vertTexCoordsDimension, GLenum vertTexCoordsUsage, const int vertIndices[], const unsigned int vertIndicesLen, GLenum vertIndicesUsage):
Shape(verticiesPos, verticiesPosLen, vertPosAttribIndex, posDimension, vertPosUsage, vertIndices, vertIndicesLen, vertIndicesUsage){

TexturedShape::texCoordsVBO = new VBO();
TexturedShape::texCoordsVBO->bindVBO();
TexturedShape::texCoordsVBO->uploadData(vertTexCoords, vertTexCoordsLen, vertTexCoordsUsage);
//VAO is already bound from the constructor of shape but just to make sure, we'll bind it again
Shape::shapeVAO->bindVAO();
TexturedShape::shapeVAO->bindAttribArray(vertTexCoordsAttribIndex);
TexturedShape::texCoordsVBO->bindVBO();
TexturedShape::shapeVAO->mapBoundVBOToAttribArray(GL_FLOAT, vertTexCoordsDimension);
TexturedShape::shapeVAO->unbindAttribArray(vertTexCoordsAttribIndex);

}



#ifndef TEXTUREDSHAPE_H
#define TEXTUREDSHAPE_H

#include "Shape.h"


class TexturedShape : public Shape{
   protected:
     VBO *texCoordsVBO;

   public:
      explicit TexturedShape(const float verticiesPos[], const unsigned int verticiesPosLen, const int vertPosAttribIndex, const int posDimension, GLenum vertPosUsage, const float vertTexCoords[], const unsigned int vertTexCoordsLen, const int vertTexCoordsAttribIndex, const int vertTexCoordsDimension, GLenum vertTexCoordsUsage, const int vertIndices[], const unsigned int vertIndicesLen, GLenum vertIndicesUsage);
      inline ~TexturedShape() override{ delete texCoordsVBO; }

      inline VBO* getTexCoordsVBO() const{ return texCoordsVBO; }

};


#endif // TEXTUREDSHAPE_H

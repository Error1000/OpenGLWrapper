#include "../../OpenGLWrapper/Include.h"
#include <unistd.h>
#include <time.h>

using namespace UserUtils;


void setup();
void render();
void onExit();
void onResize();
void setRenderingColor(Shader* shader, const string unr, const string ung, const string unb, const string una, byte r, byte g, byte b, byte a);

int main(){
try{
 Window::setWidth(400);
 Window::setHeight(400);
 Window::useVSync(true);
 Window::setFullScreen(false);
 Window::setResizeable(true);
 Window::setWindowResizeCallback(onResize);
 Wrapper::initWrapper(setup, render, onExit);
}catch(APIError a){
 Logger::log(a.message, Level::Error);
 return 0;
}catch(WindowError w){
 Logger::log(w.message, Level::Error);
 return 0;
}

Wrapper::runWrapper();
return 0;
}
//Variable declarations
const unsigned int vertPosLen = 8;
const unsigned int indicesLen = 6;


const float defaultVertPositions[vertPosLen] = {
                                     //Nr.
			//TOP RIGHT TRIANGLE
			-1, 1, //TOP LEFT          0
			1, 1, //TOP RIGHT         1
			1, -1, // BOTTOM RIGHT     2

			//BOTTOM LEFT TRIANGLE
			-1, -1 // BOTTOM LEFT      3

};


const int defaultIndices[indicesLen] = {
         //TOP RIGHT TRIANGLE
			0, //TOP LEFT
			1, //TOP RIGTH
			3, //BOTTOM LEFT

			//BOTOM LEFT TRIANGLE
			1, //TOP RIGHT
			2, //BOTTOM RIGHT
			3 //BOTTOM LEFT

};


glm::mat3 projection;
Shader* sh;
Shape* s;
DrawableObject2D* d;


void onResize(){
 projection = glm::ortho(-(float)Window::getWidth()/2, (float)Window::getWidth()/2, -(float)Window::getHeight()/2, (float)Window::getHeight()/2);
}

void setup(){
backgroundColor(255);

srand(time(NULL));


try{
 sh = new Shader(readFile(getCurrentProgramDir() + "/assets/shaderProgram/shader.vs"), readFile(getCurrentProgramDir() + "/assets/shaderProgram/shader.fs"));
}catch(ShaderError se){
 Logger::log(se.message, Level::Error);
 return;
}catch(APIError ae){
 Logger::log(ae.message, Level::Error);
 return;
}


sh->loadUniformId("r");
sh->loadUniformId("g");
sh->loadUniformId("b");
sh->loadUniformId("a");
sh->loadUniformId("useTexture");
sh->loadUniformId("objTexture");
sh->loadUniformId("transformationMatrix");
sh->loadAttributeId("positions");
sh->loadAttributeId("tex_coord");

projection = glm::ortho(-(float)Window::getWidth()/2, (float)Window::getWidth()/2, -(float)Window::getHeight()/2, (float)Window::getHeight()/2);


s = new Shape(defaultVertPositions, vertPosLen, sh->getAttributeId("positions"), 2, GL_STATIC_DRAW,
              defaultIndices, indicesLen, GL_STATIC_DRAW);

d = new DrawableObject2D(s, sh);
d->setPosition(vec2(0, 0));
d->setSize(vec2(Window::getWidth()*(2.0f/4.0f), Window::getHeight()*(2.0f/4.0f) ));


sh->bindShader();
sh->setUniform(sh->getUniformId("useTexture"), GL_FALSE);

d->getShape()->bindShape();
setRenderingColor(sh, "r", "g", "b", "a", 255, 0, 0, 255);

try{
 sh->validateShader();
}catch(ShaderError validationError){
 Logger::log(validationError.message, Level::Error);
 Logger::log("OpenGL shader validation error!");
 return;
}

}

void render(){
if(Window::isKeyPressed(GLFW_KEY_ESCAPE))exitWrapper();



d->setPosition((Window::getMousePos() - vec2(Window::getWidth()/2, Window::getHeight()/2)) * vec2(1, -1) );

//Flashing colors
//setRenderingColor(sh, "r", "g", "b", "a", rand()%(255-125)+1+125, rand()%255+1, rand()%255+1, 255);

d->rotate(  glm::radians( Window::getAverageDelta() * 100 * ((rand()%2 == 0) ? 1 : -1) ) );


d->getShader()->setMatrixUniform(d->getShader()->getUniformId("transformationMatrix"), projection * d->getModelMatrix());
d->renderObject();
}

void onExit(){
 delete sh;
 delete s;
 delete d;
}

void setRenderingColor(Shader* shader, const string unr, const string ung, const string unb, const string una, byte r, byte g, byte b, byte a){
shader->setUniform(shader->getUniformId(unr), r/255.0f);
shader->setUniform(shader->getUniformId(ung), g/255.0f);
shader->setUniform(shader->getUniformId(unb), b/255.0f);
shader->setUniform(shader->getUniformId(una), a/255.0f);
}



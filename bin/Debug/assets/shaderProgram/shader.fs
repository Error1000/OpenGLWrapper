//Set the current version so that opengl knows which version of the compiler to use to compile this
#version 330 core

//Input
//Note this time we don't get the vertices attribute since when we give the data we only give it to the vertex shader
//But what we do need to make is the uniform that we will use
uniform float r,g,b,a;
uniform bool useTexture;
uniform sampler2D objTexture;

//Shader variables
//Input
in vec2 pass_tex_coord;
//Output
out vec4 FragmentColor;

void main() {
   //Set the color of the quad
    if(useTexture != true){
     FragmentColor =  vec4( r, g, b, a);
    }else{
     FragmentColor = texture(objTexture, pass_tex_coord);
    }

}

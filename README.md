# OpenGLWrapper
This is a sort of adaptation of my java lwjgl wrapper but for c++ and it's better made this time. 

NOTE: This git repository uses submoduels so to clone it you need to add the arguments: --recurse-submodules -j8 to the git clone command!!!
NOTE: Since this repostiory uses submodules DO NOT download it using the web interface it does not downlaod submodules isntead clone the project with the arguments: --recurse-submodules -j8 !!!

Here's a simple example for opening a window:

```
 #include <OpenGLWrapper/Include.h>
 void setup();
 void render();
 
 using namespace UserUtils;
 
 int main(){
   try{
    Window::setTitle("Game");

    Wrapper::initWrapper(setup, render); 
   }catch(APIError a){
    Logger::log(a.message, Level::Error);
    return 0;
   }catch(WindowError w){
    Logger::log(w.message, Level::Error);
    return 0;
   }

   Wrapper::runWrapper();
 }
 
 void setup(){
   backgroundColor(255);
 }
 
 void render(){
  if(Window::isKeyPressed(GLFW_KEY_ESCAPE)) exitWrapper();
 }
```

And here's how it looks:

![Example](screenshot/Example.png)

# How to build the release (build release library binary)

Note: The automatron commands must be run from the project folder where this README is located!!!

 # GNU/Linux (using make to build and the default package manager to get libraries and install stuff)
  Note: This command will download and install the required applications and libraries needed to build.

  - Ubuntu: sudo ./automatron9000/automatron9000.sh -m release -o ubuntu -l "$(cat Libraries/linux/ubuntu/ilibs)"

  - Arch: sudo ./automatron9000/automatron9000.sh -m release -o arch -l "$(cat Libraries/linux/arch/ilibs)"

 # Windows (using make to build and the chocolatey package manager to install stuff and using the libraries from the Libraries\windows folder provided)
  Note: This command will download and install the required applications needed to build this project and it will also install chocolatey. 
  
  - Go to Libraries\windows, make sure that all zips are unzipped if not, unzip and put all the contents of the zipps in the same folder
  - Open cmd as administrator
  - Run: automatron9000\automatron9000.bat -m release -o windows
 
 # MacOS (using make to build and brew package manager to get libraries and install stuff)
  Note: This command will download and install the required applications needed to build and install brew. 

  - ./automatron9000/automatron9000.sh -m release -o macos -l "$(cat Libraries/macos/ilibs)"


# How to test build (build debug executable binary)

Note: The automatron commands must be run from the project folder where this README is located!!!

 # GNU/Linux (using make to build and assuming you already built the release and have all the stuff necessary from building the release)
  - Ubuntu: ./automatron9000/automatron9000.sh -m debug -o ubuntu
  
  - Arch: ./automatron9000/automatron9000.sh -m debug -o arch

 # Windows (using make to build and assuming you already built the release and have all the stuff necessary from building the release)
  - Go to Libraries\windows, make sure that all zips are unzipped if not, unzip and put all the contents of the zipps in the same folder
  - Open cmd as administrator
  - Run: automatron9000\automatron9000.bat -m debug -o windows

 # MacOS (using make to build and assuming you already built the release and have all the stuff necessary from building the release)
  - ./automatron9000/automatron9000.sh -m debug -o macos
